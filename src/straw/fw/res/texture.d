﻿module straw.fw.res.texture;
version( graphics ):

import derelict.opengl3.gl3;
import derelict.sdl2.image;
import derelict.sdl2.sdl;
import std.conv;
import std.exception;
import std.string;
import straw.core;
import straw.core.utils.flags;
import straw.fw.graph.renderer;
import straw.fw.res.advTexRenderer;
import straw.fw.res.resource;

public import straw.fw.res.advTexRenderer;

final class Texture : Resource {
	mixin( StrawObject );
	
private:
	package( straw.fw )
		GLuint textureId;

	SDL_Surface *sdlSurface;
	V2G size_;

private:
	this( string filename, bool repeat, bool isShared ) {
		super( true );

		sdlSurface = IMG_Load( ( "../res/image/" ~ filename ).toStringz );
		enforce( sdlSurface, "Sdl texture load fail: " ~ filename );
		
		size_.assign( sdlSurface.w, sdlSurface.h );

		int format;
		switch( sdlSurface.format.BytesPerPixel ) {

			case 4:
				format = GL_RGBA;
				break;

			case 3:
				format = GL_RGB;
				break;

			default:
				assert( 0, "Unknown sdl surface format" );

		}

		glGenTextures( 1, &textureId );
		enforce( textureId > 0, "GL texture creation failed" );

		renderer.gl.texture = this;

		glTexImage2D( GL_TEXTURE_2D, 0, format, sdlSurface.w, sdlSurface.h, 0, format, GL_UNSIGNED_BYTE, sdlSurface.pixels );
		setParams( repeat );

		SDL_FreeSurface( sdlSurface );
	}
	this( V2G size, bool repeat = true ) {
		super( false );

		size_ = size;

		glGenTextures( 1, &textureId );
		enforce( textureId > 0, "GL texture creation failed" );

		renderer.gl.texture = this;

		glTexImage2D( GL_TEXTURE_2D, 0,  GL_RGBA, cast( GLsizei ) size.x, cast( GLsizei ) size.y, 0,  GL_RGBA, GL_UNSIGNED_BYTE, null );
		setParams( repeat );
	}
	~this() {
		// TODO - textures delete list
		if( textureId )
			glDeleteTextures( 1, &textureId );
	}

public:
	static This create( V2G size, bool repeat = true ) {
		return new Texture( size, true );
	}
	static This createFromFile( string filename, bool repeat = true, bool isShared = true ) {
		string key = "TEXTURE#filename=" ~ filename ~ "#repeat=" ~ repeat.to!string;
		Texture tex;

		if( isShared ) {
			tex = resourceManager.get!Texture( key );

			if( tex )
				return tex;
		}

		tex = new Texture( filename, repeat, isShared );
		resourceManager.register( key, tex );

		return tex;
	}

public:
	@property V2G size() const {
		return size_;
	}

	void resize( V2G newSize ) {
		renderer.gl.texture = this;
		size_ = newSize;

		glTexImage2D( GL_TEXTURE_2D, 0,  GL_RGBA, cast( GLsizei ) size.x, cast( GLsizei ) size.y, 0,  GL_RGBA, GL_UNSIGNED_BYTE, null );
	}

public:
	void draw( V2G pos ) {
		with( renderer.gl ) {
			setDrawMode_texture( this );
			commonVertexBuffer.pushRect( pos, size_ );
			commonTextureCoordBuffer.pushRect( 0.V2G, 1.V2G );
		}
	}
	auto advancedDraw( V2G pos ) {
		return AdvancedTextureRenderer!()( this, pos );
	}

private:
	void setParams( bool repeat ) {
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

		if( repeat ) {
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

		} else {
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		}
	}
	
}

AdvancedTextureRenderer!() renderTextueAdvanced( GLuint textureId, V2G textureSize, V2G pos ) {
	return AdvancedTextureRenderer!()( textureId, textureSize, pos );
}