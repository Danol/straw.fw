﻿module straw.fw.res.shader;
version( graphics ):

import derelict.opengl3.gl3;
import std.conv;
import std.exception;
import std.file;
import std.stdio;
import std.string;
import straw.core;
import straw.core.cfx;
import straw.core.exception;
import straw.fw.res.resource;

final class Shader : Resource {
	mixin( StrawObject );

private:
	package( straw.fw )	GLuint programId;

	GLint[ string ] attributes, uniforms;

	bool hasFragment, hasVertex;
	GLint fragmentId, vertexId;
	string identifier;

private:
	this( string name ) {
		super( true );

		identifier = name;

		CfxObject data = CfxObject.createFromFile( "../res/shader/" ~ name ~ ".cfx" );

		foreach( attr; data[ "attributes" ].toArray )
			attributes[ attr.toStr ] = -1;

		foreach( uni; data[ "uniforms" ].toArray )
			uniforms[ uni.toStr ] = -1;

		programId = glCreateProgram();

		if( auto obj = "fragmentCode" in data ) {
			hasFragment = true;
			compileShader( fragmentId, GL_FRAGMENT_SHADER, obj.toStr );
		}

		if( auto obj = "vertexCode" in data ) {
			hasVertex = true;
			compileShader( vertexId, GL_VERTEX_SHADER, obj.toStr );
		}

		linkProgram();

		// Find uniforms
		foreach( name, ref value; uniforms ) {
			value = glGetUniformLocation( programId, name.toStringz );
			//enforce( value != -1, new StrawException( "shaderMissingUniform", this, "uniformName", name ) );
		}

		alias nm = name;
		// Find attributes
		foreach( name, ref value; attributes ) {
			value = glGetAttribLocation( programId, name.toStringz );
			//enforce( value != -1, new StrawException( "shaderMissingAttribute", this, "attributeName", name ) );
		}
	}
	~this() {
		if( fragmentId != fragmentId.init )
			glDeleteShader( fragmentId );

		if( vertexId != vertexId.init )
			glDeleteShader( vertexId );

		if( programId != programId.init )
			glDeleteProgram( programId );
	}

public:
	static Shader createFromFile( string name ) {
		string key = "SHADER#" ~ name;
		Shader sh = resourceManager.get!Shader( key );

		if( sh )
			return sh;
			
		sh = new Shader( name );
		resourceManager.register( key, sh );

		return sh;
	}

public:
	GLint uniformId( string identifier ) {
		assert( identifier in uniforms, "Shader '" ~ this.identifier ~ "' is missing uniform '" ~ identifier ~ "'" );
		return uniforms[ identifier ];
	}
	GLint attributeId( string identifier ) {
		assert( identifier in attributes, "Shader '" ~ this.identifier ~ "' is missing attribute '" ~ identifier ~ "'" );
		return attributes[ identifier ];
	}

	GLint uniformId( string identifier )() {
		// TODO Init time lookup table
		assert( identifier in uniforms, "Shader '" ~ this.identifier ~ "' is missing uniform '" ~ identifier ~ "'" );
		return uniforms[ identifier ];
	}
	GLint attributeId( string identifier )() {
		// TODO Init time lookup table
		assert( identifier in attributes, "Shader '" ~ this.identifier ~ "' is missing attribute '" ~ identifier ~ "'" );
		return attributes[ identifier ];
	}

private:
	void compileShader( ref GLint id, GLuint shaderType, string code ) {
		auto cCode = code.toStringz;
		scope( exit ) delete cCode;

		GLint shaderId = glCreateShader( shaderType );
		glShaderSource( shaderId, 1, &cCode, null );
		glCompileShader( shaderId );

		GLint result = GL_FALSE;
		glGetShaderiv( shaderId, GL_COMPILE_STATUS, &result );
		
		if( result != GL_TRUE ) {
			GLint len;
			glGetShaderiv( shaderId, GL_INFO_LOG_LENGTH , &len );   
			
			char[] log = new char[ len ];
			scope( exit ) delete log;

			glGetShaderInfoLog( shaderId, len, null, log.ptr );
			assert( 0, "Shader complation error (" ~ log.to!string ~ ")" );
		}
		
		glAttachShader( programId, shaderId );
	}
	void linkProgram() {
		glLinkProgram( programId );
		
		GLint result = GL_FALSE;
		glGetProgramiv( programId, GL_LINK_STATUS, &result );
		
		if( result != GL_TRUE ) {
			GLint len;
			glGetProgramiv( programId, GL_INFO_LOG_LENGTH, &len );

			char[] log = new char[ len ];
			scope( exit ) delete log;

			glGetProgramInfoLog( programId, len, null, log.ptr );
			throw new StrawException( "shaderLinkError", this, "error", log.to!string );
		}
	}

public:
	string[string] __exceptionInfo() {
		return [
			"registeredAs": registeredInResourceManager
		];
	}

}