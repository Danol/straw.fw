﻿module straw.fw.res.resource;
version( graphics ):

import straw.core;
import straw.fw.app;
import straw.fw.graph.renderer;

alias hookLoadSystemResources = hook!"loadSystemResources";
alias hookUnloadResources = hook!"unloadResources";

/// Rather for internal stuff
package	ResourceManager resourceManager;

mixin registerDistributedTLSVariable!resourceManager;

abstract class Resource {
	mixin( StrawObject );

private:
	bool isShared_;
	string registeredInResourceManager_;

protected:
	this( bool isShared ) {
		isShared_ = isShared;
	}
	~this() {
		if( !resourceManager.isDestroying && registeredInResourceManager_ )
			resourceManager.resourceTable.remove( registeredInResourceManager_ );
	}

public:
	final @property bool isShared() const {
		return isShared_;
	}
	final @property string registeredInResourceManager() const {
		return registeredInResourceManager_;
	}

}

final class ResourceManager {
	mixin( StrawObject );

private:
	Resource[ string ] resourceTable;
	bool isDestroying;

public:
	~this() {
		isDestroying = true;

		// Delete all resources
		foreach( res; resourceTable )
			delete res;
	}

public:
	void register( string key, Resource resource ) {
		assert( key !in resourceTable, "Resource with key '" ~ key ~ "' already registered." );

		resourceTable[ key ] = resource;
		resource.registeredInResourceManager_ = key;
	}
	T get( T : Resource )( string key ) {
		if( auto ret = key in resourceTable )
			return cast( T ) *ret;

		else
			return null;
	}

}

mixin _moduleInit!({
		hookGLAppInit.add( "resourceManager" ) = {
			resourceManager = new ResourceManager();

			hookLoadSystemResources.call();
		};
		hookGLAppUninit.add( "resourceManager" ) = {
			hookUnloadResources.call();

			delete resourceManager;
		};
	});