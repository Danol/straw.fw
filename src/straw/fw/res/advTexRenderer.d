﻿module straw.fw.res.advTexRenderer;

import derelict.opengl3.types;
import std.algorithm;
import std.array;
import std.conv;
import std.meta;
import straw.core.utils.flags;
import straw.core.utils.typetuple;
import straw.fw.graph.glWrapper;
import straw.fw.graph.renderer;
import straw.fw.res.texture;

private alias ATRFlags = straw.core.utils.flags.Flags!"advancedTextureRenderer";
struct AdvancedTextureRenderer( ATRFlags flags_ = ATRFlags() ) {
	
public:
	alias Flags = ATRFlags;
	enum flags = flags_;
	
private:
	GLuint textureId;
	V2G pos, textureSize;
	debug bool finished;
	
private:
	mixin( { return flags.bitsArray.map!( x => "AggregateDataByIndex!" ~ x.to!string ~ " aggr_" ~ x.to!string ~ ";" ).join( '\n' ); }() );
	
public:
	@disable this();
	this( Texture texture, V2G pos ) {
		this.textureId = texture.textureId;
		this.textureSize = texture.size;
		this.pos = pos;
	}
	this( GLuint textureId, V2G textureSize, V2G pos ) {
		this.textureId = textureId;
		this.textureSize = textureSize;
		this.pos = pos;
	}
	private this( Other, AggrData )( Other other, AggrData aggrData )	{
		enum otherFlags = Other.flags;

		textureId = other.textureId;
		textureSize = other.textureSize;
		pos = other.pos;
		
		// Copy data from previous helper
		mixin( otherFlags.bitsArray.map!( id => "aggr_" ~ id.to!string ~ " = other.aggr_" ~ id.to!string ~ ";" ).join( '\n' ) );

		enum addedAggregate = ( flags - otherFlags ).firstBit;
		
		// Get new data from constructor args
		mixin( "aggr_" ~ addedAggregate.to!string ~ " = aggrData;" );
	}
	~this() {
		debug assert( finished, "AdvancedDraw not finished." );
	}
	
public:
	AdvancedTextureRenderer!( flags + aggregateFlag!ATRA_part ) part()( V2G size, V2G offset = 0.V2G ) {
		return aggregateFunc!( ATRA_part )( size, offset );
	}
	auto scale()( V2G size ) {
		return aggregateFunc!( ATRA_scale )( size );
	}
	auto flip()( bool horizontally, bool vertically ) {
		return aggregateFunc!( ATRA_flip )( horizontally, vertically );
	}

	auto drawMode()( GLWrapper.DrawModeFunction mode ) {
		return aggregateFunc!( ATRA_drawMode )( mode );
	}
	
private:
	auto aggregateFunc( Aggr, Args ... )( Args args ) {
		static assert( ( flags - aggregateCompatibleFlags!Aggr ).empty, "Incompatible advancedDraw arguments: '" ~ Aggr.stringof ~ "' is not compatible with " ~ aggregateStringListFromFlags!( flags - aggregateCompatibleFlags!Aggr ) );
		
		debug finished = true;
		return AdvancedTextureRenderer!( flags + aggregateFlag!Aggr )( this, Aggr.Data( args ) );
	}
	template aggregateData( Aggr )
		if( aggregateFlag!Aggr in flags )
	{
		mixin( "alias aggregateData = aggr_" ~ aggregateIndex!Aggr.to!string ~ ";" );
	} 
	
public:
	void finish()() {
		debug assert( !finished, "Sanity check fail" );
		debug finished = true;

		static if( aggregateFlag!ATRA_drawMode in flags ) {
			if( renderer.gl.texture != textureId )
				renderer.gl.finishDrawMode();

			renderer.gl.drawMode = aggregateData!ATRA_drawMode.mode;
			renderer.gl.texture = textureId;

		} else
			renderer.gl.setDrawMode_texture( textureId );
		
		V2G texCoordLT = 0.V2G;
		V2G texCoordRB = 1.V2G;
		
		static if( aggregateFlag!ATRA_scale in flags )
			V2G size = aggregateData!ATRA_scale.size;

		else static if( aggregateFlag!ATRA_part in flags )
			V2G size = aggregateData!ATRA_part.size;

		else
			V2G size = textureSize;
		
		static if( aggregateFlag!ATRA_part in flags ) {
			texCoordLT = aggregateData!ATRA_part.offset / textureSize;
			texCoordRB = texCoordLT + aggregateData!ATRA_part.size / textureSize;
		}

		static if( aggregateFlag!ATRA_flip in flags ) {
			if( aggregateData!ATRA_flip.horizontally ) {
				texCoordLT.x = 1 - texCoordLT.x;
				texCoordRB.x = 1 - texCoordRB.x;
			}
			
			if( aggregateData!ATRA_flip.vertically ) {
				texCoordLT.y = 1 - texCoordLT.y;
				texCoordRB.y = 1 - texCoordRB.y;
			}
		}
		
		renderer.gl.commonVertexBuffer.pushRect( pos, size );
		renderer.gl.commonTextureCoordBuffer.pushRectLTRB( texCoordLT, texCoordRB );
	}
	
}

private {
	alias Aggregates = AliasSeq!( ATRA_part, ATRA_scale, ATRA_flip, ATRA_drawMode );
	
	final abstract class ATRA_part {
	public:
		alias compatibleAggregates = AliasSeq!( ATRA_scale, ATRA_flip, ATRA_drawMode );
		static struct Data {
			V2G size, offset;
		}
	}
	final abstract class ATRA_scale {
		public:
		alias compatibleAggregates = AliasSeq!( ATRA_part, ATRA_flip, ATRA_drawMode );
		static struct Data {
			V2G size;
		}
	}
	final abstract class ATRA_flip {
		public:
		alias compatibleAggregates = AliasSeq!( ATRA_part, ATRA_scale, ATRA_drawMode );
		static struct Data {
			bool horizontally, vertically;
		}
	}
	final abstract class ATRA_drawMode {
	public:
		alias compatibleAggregates = AliasSeq!( ATRA_part, ATRA_scale, ATRA_flip );
		static struct Data {
			GLWrapper.DrawModeFunction mode;
		}
	}
	

	template AggregateDataByIndex( ubyte index ) {
		alias Aggr = Aggregates[ index ];

		alias AggregateDataByIndex = Aggr.Data;
	}

	enum size_t aggregateIndex( Aggr ) = staticIndexOf!( Aggr, Aggregates );
	enum ATRFlags aggregateFlag( Aggr ) = {
		enum ind = aggregateIndex!Aggr;
		static assert( ind != -1, Aggr.stringof ~ " is not a valid aggregate." );

		return ATRFlags.bit!ind;
	} ();
	
	enum ATRFlags aggregateCompatibleFlags( Aggr ) = {
		ATRFlags result;
		foreach( compatAggr; Aggr.compatibleAggregates ) {
			static assert( staticIndexOf!( Aggr, compatAggr.compatibleAggregates ) != -1, "Compatibility list broken for '" ~ Aggr.stringof ~ "' and '" ~ compatAggr.stringof ~ "'" );

			result += aggregateFlag!compatAggr;
		}
		return result;
	}();
	enum string aggregateStringListFromFlags( ATRFlags flags ) = {
		string result;
		foreach( bit; Tuplify!( flags.bitArray ) ) {
			if( result ) result ~= ", ";
			result ~= Aggregates[ bit ].stringof;
		}
		return result;
	}();
}