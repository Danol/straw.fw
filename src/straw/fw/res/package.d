﻿module straw.fw.res;
version( graphics ):

public {
	import straw.fw.res.resource;
	import straw.fw.res.shader;
	import straw.fw.res.texture;
	import straw.fw.res.advTexRenderer;
}