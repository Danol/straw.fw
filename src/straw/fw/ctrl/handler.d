﻿module straw.fw.ctrl.handler;

import std.algorithm;
import straw.core.exception;
import straw.core.object;
import straw.fw.app;
import straw.fw.ctrl.device.device;
import straw.fw.ctrl.mgr;

struct ControlHandler {

private:
	bool value, prevValue;

private:
	ControlsDeviceInput main;
	ControlsDeviceInput[] operators;

public:
	final @property size_t operatorCount() const {
		return operators.length;
	}

public:
	@property bool isPressed() const {
		return value;
	}
	@property bool isPress() const {
		return value && !prevValue;
	}
	@property bool isRelease() const {
		return !value && prevValue;
	}

public:
	/// Link the handler using string-encoded inputs (deviceName.inputName)
	void setCombination( in string main, in string[] operators ... ) {
		this.main = controlsMgr.resolveInput( main );
		
		foreach( op; operators )
			this.operators ~= controlsMgr.resolveInput( op );
	}

	void step() {
		if( main.handlerToBeOpCount <= operatorCount && operators.all!"a.value" ) {
			if( !main.handlerToBe )
				controlsMgr.markInputToBeHandled( main );

			main.handlerToBe = &this;
			main.handlerToBeOpCount = operatorCount;
		}

		prevValue = value;

		if( main.lastChangeTick == app.tickCounter ) {
			// If this handler is handling the input -> take the value
			if( main.currentHandler == &this )
				value = main.value;
			
			// If this handler is not handling the input, but the input is false, set this handler also to false
			else if( value && !main.value )
				value = false;
		}
	}

}