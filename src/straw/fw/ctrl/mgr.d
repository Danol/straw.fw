﻿module straw.fw.ctrl.mgr;

import derelict.sdl2.types;
import std.exception;
import std.string;
import straw.core;
import straw.core.exception;
import straw.fw.app;
import straw.fw.ctrl.device.device;
import straw.fw.graph.renderer;
import straw.fw.ctrl.handler;
import std.container.array;

alias hookRegisterControlsDevices = hook!"registerControlsDevices";

alias ControlsException = DerivedStrawException!( "controls" );
	
ControlsManager controlsMgr;
V2G cursorPos;

final class ControlsManager {
	
private:
	ControlsDevice[ string ] devices;
	Array!ControlsDeviceInput inputsToBeHandled;
	
private:
	~this() {
		foreach( dev; devices )
			delete dev;
	}

protected:
	void step() {
		foreach( ControlsDeviceInput it; inputsToBeHandled ) {
			with( it ) {
				currentHandler = handlerToBe;
				handlerToBe = null;
				handlerToBeOpCount = 0;
			}
		}
		inputsToBeHandled.clear();
	}

package:
	void registerDevice( ControlsDevice device ) {
		assert( device !is null );
		enforce( device.name !in devices, new StrawException( "deviceNameConflict", "deviceName", device.name ) );

		devices[ device.name ] = device;
	}

	/// Resolves input name (deviceName.inputName) and returns corresponding ControlsDeviceInput (throws exception if failed)
	ControlsDeviceInput resolveInput( in string name ) {
		size_t separatorPos = name.indexOf('.');
		enforce( separatorPos != -1, new ControlsException( "inputResolutionFail", "inputName", name, "reason", "dotMissing" ) );

		string deviceName = name[ 0 .. separatorPos ];
		ControlsDevice *device = deviceName in devices;
		enforce( device, new ControlsException( "inputResolutionFail", "inputName", name, "device", deviceName, "reason", "deviceDoesNotExist" ) );

		try
			return device.inputByName( name[ separatorPos + 1 .. $ ] );

		catch( ControlsException exc )
			throw exc.addArguments( "inputName", name, "reason", exc.identifier ).changeIdentifier( "inputResolutionFail" );
	}

package( straw.fw ):
	bool handleSdlEvent( in ref SDL_Event ev ) {
		if( ev.type == SDL_MOUSEMOTION ) {
			cursorPos.assign( ev.motion.x, ev.motion.y );
			return true;
		}

		foreach( dev; devices ) {
			if( dev.handleSdlEvent( ev ) )
				return true;
		}

		return false;
	}

package:
	void markInputToBeHandled( ControlsDeviceInput input ) {
		inputsToBeHandled ~= input;
	}

}

mixin _moduleInit!({
		hookGLAppInit.add( "controls" ).callBefore( "resourceManager" ) = {
			controlsMgr = new ControlsManager;

			hookRegisterControlsDevices.call();
		};
		hookGLAppBeforeDraw.add( "controls" ) = {
			controlsMgr.step();
		};
		hookGLAppUninit.add( "controls" ) = {
			delete controlsMgr;
		};
	});