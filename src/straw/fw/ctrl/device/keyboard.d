﻿module straw.fw.ctrl.device.keyboard;

import derelict.sdl2.types;
import std.exception;
import straw.core.utils.typetuple;
import straw.fw.app.app;
import straw.fw.ctrl.device.device;
import straw.fw.ctrl.mgr;
import straw.core;

enum keyCodesByAlias = {
	SDL_Keycode[ string ] result = [
		"lCtrl": SDLK_LCTRL,
		"rCtrl": SDLK_RCTRL,

		"lAlt": SDLK_LALT,
		"rAlt": SDLK_RALT,

		"up": SDLK_UP,
		"down": SDLK_DOWN,
		"left": SDLK_LEFT,
		"right": SDLK_RIGHT
	];

	foreach( ch; Tuplify!( "abcdefghijklmnopqrstuvwxyz" ) )
		result[ [ch] ] = mixin( "SDLK_" ~ ch );

	return result;
} ();

final class ControlsDevice_Keyboard : ControlsDevice {

private:
	ControlsDeviceInput[ SDL_Keycode ] inputsByKeyCodes;

public:
	this() {
		super( "keyboard" );
	}

public:
	override bool handleSdlEvent( in ref SDL_Event ev ) {
		if( ev.type != SDL_KEYUP && ev.type != SDL_KEYDOWN )
			return false;

		if( ControlsDeviceInput* inp = ev.key.keysym.sym in inputsByKeyCodes ) {
			bool targetValue = ( ev.type == SDL_KEYDOWN ) ? 1 : 0;

			if( targetValue != (*inp).value ) {
				(*inp).value = targetValue;
				(*inp).lastChangeTick = app.tickCounter;
			}
		}

		return true;
	}
	
protected:
	/// Throws an exception if teased
	override void registerNewInput( ControlsDeviceInput input, in string name ) {
		SDL_Keycode *key = name in keyCodesByAlias;
		enforce( key, new ControlsException( "inputDoesNotExistInDevice", "key", name, "device", this.name ) );

		inputsByKeyCodes[ *key ] = input;
	}

}

mixin _moduleInit!({
		hookRegisterControlsDevices.add( "keyboard" ) = {
			controlsMgr.registerDevice( new ControlsDevice_Keyboard );
		};
	});