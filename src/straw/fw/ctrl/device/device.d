﻿module straw.fw.ctrl.device.device;

import derelict.sdl2.types;
import straw.fw.ctrl.handler;

abstract class ControlsDevice {

private:
	string name_;
	ControlsDeviceInput[ string ] inputsByNames;

public:
	this( string name ) {
		name_ = name;
	}

public:
	final @property string name() {
		return name_;
	}

public:
	/// Returns input by it's identifier. If the input doesn't exist, creates one.
	final ControlsDeviceInput inputByName( in string name ) {
		if( auto result = name in inputsByNames )
			return *result;
		
		ControlsDeviceInput result = new ControlsDeviceInput();
		inputsByNames[ name ] = result;
		registerNewInput( result, name );
		return result;
	}

public:
	/// Returns true if the event has been handled by the device (no further search for handler needed).
	abstract bool handleSdlEvent( in ref SDL_Event ev );

protected:
	/// Throws an exception if teased
	abstract void registerNewInput( ControlsDeviceInput input, in string name );

}

final class ControlsDeviceInput {

public:
	/// Tick of when the input last changed it's value
	size_t lastChangeTick;

	/// Current value of the input
	bool value;

	/// Control handler that has won handling of the device for current step
	ControlHandler* currentHandler;

	/// A control handler that is currently the best candidate for handling the next step of the control. Changes during the step.
	ControlHandler* handlerToBe;

	/// Operator count of the best candidate for the next step handling.
	size_t handlerToBeOpCount;

}