﻿module straw.fw.app.thread;

import core.thread;
import core.sync.mutex;
import core.sync.condition;
import std.datetime;
import straw.core;
import straw.fw.app.app;

alias hookThreadInit = hook!"threadInit"; /// Is called after ManagedThread._start
alias hookThreadUninit = hook!"threadUninit"; /// Is called before ManagedThread._stop

ManagedThread thread; /// Thread-local variable pointing to current thread
float deltaTime; /// (s) Thread-local variable which states the time difference between current and previous step of ManagedThread in seconds

abstract class ManagedThread {
	mixin( StrawObject );

private:
	Mutex stopWaitMutex;
	Condition stopWaitCondition;

private:
	Thread coreThread;
	bool shouldRun_, isRunning_;
	size_t tickCounter_;
	string name_;

	StopWatch watch;

private:
	DistributedTLSVarStorage[] distributedTLSVarTmpList;

public:
	this( string name ) {
		name_ = name;

		stopWaitMutex = new Mutex();
		stopWaitCondition = new Condition( stopWaitMutex );
	}
	~this() {
		stopWait();

		delete stopWaitCondition;
		delete stopWaitMutex;
	}

public:
	final @property size_t tickCounter() {
		return tickCounter_;
	}

protected:
	void _init() {};
	void _step() {};
	void _uninit() {};

public:
	final @property bool shouldRun() const {
		return shouldRun_;
	}
	final @property bool isRunning() {
		return isRunning_;
	}

public:
	/// Starts the loop in current thread (so no thread is created)
	final void startHere() {
		runProcedure();
	}
	/// Starts the loop in a new thread
	final void start() {
		assert( !coreThread );

		distributedTLSVarTmpList.length = __DistributedTLSVarStorageList.length;
		foreach( i; 0 .. distributedTLSVarTmpList.length )
			distributedTLSVarTmpList[ i ] = __DistributedTLSVarStorageList[ i ]();

		coreThread = new Thread( &threadedRunProcedure );
		coreThread.start();
	}

	final void stop() {
		if( isRunning )
			shouldRun_ = false;
	}

	final void stopWait() {
		if( isRunning ) {
			stop();

			synchronized( stopWaitMutex )
				stopWaitCondition.wait();
		}
	}

private:
	final void threadedRunProcedure() {
		foreach( var; distributedTLSVarTmpList ) {
			var.load();
			delete var;
		}

		distributedTLSVarTmpList = null;

		runProcedure();

		coreThread = null;
	}
	final void runProcedure() {
		thread = this;
		deltaTime = 0;

		shouldRun_ = true;
		isRunning_ = true;
		
		_init();
		hookThreadInit.call();

		watch.start();
		while( shouldRun ) {
			_step();

			deltaTime = watch.peek.nsecs * 1e-9f;
			watch.reset();
			tickCounter_ ++;
		}

		hookThreadUninit.call();
		_uninit();

		synchronized( stopWaitMutex )
			stopWaitCondition.notifyAll();

		isRunning_ = false;
	}

}

/// Registers a TLS variable as distributed. That means, when creating a new ManagedThread, variable's value is distributed from the thread the new thread was created in
mixin template registerDistributedTLSVariable( alias var )
	if( __traits( compiles, typeof( var ) ) )
{
	private alias __registerDistributedTLSVariable = __SharedTLSvarRegisterer!var;
}

private abstract class DistributedTLSVarStorage {

public:
	abstract void load();

}
private __gshared DistributedTLSVarStorage function()[] __DistributedTLSVarStorageList;

template __SharedTLSvarRegisterer( alias var ) {
	private final class DistributedTLSVarStorageImpl : DistributedTLSVarStorage {

	public:
		alias T = typeof( var );

	private:
		T tmpVar;

	public:
		this() {
			tmpVar = var;
		}

	public:
		override void load() {
			var = tmpVar;
		}

	}

	shared static this() {
		__DistributedTLSVarStorageList ~= { return new DistributedTLSVarStorageImpl; };
	}
}