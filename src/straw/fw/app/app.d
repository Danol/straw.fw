﻿module straw.fw.app.app;

import std.file;
import straw.core;
import straw.core.cfx;
import straw.fw.app.thread;

alias hookAppInit = hook!"appInit";
alias hookAppStep = hook!"appStep";
alias hookAppUninit = hook!"appUninit";

/// A TLS variable, pointing to the current Application instance
Application app;

mixin registerDistributedTLSVariable!app;

abstract class Application : ManagedThread {
	mixin( StrawObject );

public:
	CfxObject settings;

public:
	this() {
		super( "application" );
	}

protected:
	override void _init() {
		assert( !app );

		app = this;
		settings = CfxElement.createFromFile( "../cfg/settings.cfx" );

		hookAppInit.call();
	}
	override void _step() {
		hookAppStep.call();
	}
	override void _uninit() {
		hookAppUninit.call();
	}

}