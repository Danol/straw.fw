﻿module straw.fw.graph.glDraw;

import straw.fw.graph.color;
import straw.fw.graph.glWrapper;
import straw.fw.graph.renderer;

package struct GLDraw {

public:
	void line( V2G p1, V2G p2, Color c, V2G.T width = 1 ) {
		if( !c )
			return;
		
		renderer.gl.setDrawMode_color();
		
		// I have to start half the pixel before and half the pixel after
		V2G dirHalf = ( p2 - p1 ).normalized / 2;
		
		V2G gp1 = p1 - dirHalf + 0.5;
		V2G gp2 = p2 + dirHalf + 0.5;
		
		V2G normal = V2G( gp2.y - gp1.y, gp1.x - gp2.x ).normalized * width / 2;
		
		with( renderer.gl.commonVertexBuffer ) {
			push( gp1 + normal );
			push( gp2 + normal );
			push( gp1 - normal );
			
			push( gp1 - normal );
			push( gp2 + normal );
			push( gp2 - normal );
		}
		renderer.gl.commonColorBuffer.pushNTimes!6( c );
	}


public:
	void rectangle( V2G pos, V2G size, Color color ) {
		with( renderer.gl ) {
			setDrawMode_color();

			commonVertexBuffer.pushRect( pos, size );
			commonColorBuffer.pushNTimes!6( color );
		}
	}

}