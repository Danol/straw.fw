﻿module straw.fw.graph.renderer;
version( graphics ):

import derelict.opengl3.gl3;
import derelict.sdl2.image;
import derelict.sdl2.sdl;
import derelict.sdl2.ttf;
import std.conv;
import std.exception;
import std.string;
import straw.core;
import straw.core.math.viewMatrix;
import straw.core.utils.general;
import straw.fw.app.app;
import straw.fw.app.thread;
import straw.fw.ctrl.mgr;
import straw.fw.graph;

alias Vec2Graphics = Vec2!float;
alias Vec3Graphics = Vec3!float;

alias V2G = Vec2Graphics;
alias V3G = Vec3Graphics;


alias hookGLAppInit = hook!"GLAppInit";
alias hookGLAppUninit = hook!"GLAppUninit";

alias hookResolutionChange = hook!"resolutionChange";

alias hookGLAppBeforeDraw = hook!"GLAppBeforeDraw";
alias hook2DDraw = hook!"2DDraw";

/// Interface for rendering stuff
Renderer renderer;

/// ditto
final class Renderer {
	mixin( StrawObject );

private:
	SDL_Window *sdlWindow;
	SDL_GLContext glContext;
	V2G resolution_;

public:
	GLWrapper gl;

package:
	IRenderTarget firstResolutionSensitiveRenderTarget;

public:
	static __gshared GLDraw draw;

public:
	@property V2G resolution() const {
		return resolution_;
	}
	@property void resolution( V2G set ) {
		if( resolution == set ) return;

		resolution_ = set;

		SDL_SetWindowSize( sdlWindow, cast( int ) set.x, cast( int ) set.y );
		glViewport( 0, 0, cast( int ) set.x, cast( int ) set.y );

		IRenderTarget target = firstResolutionSensitiveRenderTarget;
		while( target ) {
			target.updateResolution();
			target = target.nextResolutionSensitiveRenderTarget;
		}
		
		hookResolutionChange.call();
	}

public:
	this() {
		gl = GLWrapper( None() );
	}

public:
	void pushRenderTarget( IRenderTarget target ) {
		gl.pushRenderTarget( target );
	}
	void popRenderTarget() {
		gl.popRenderTarget();
	}

private:
	void _init() {
		resolution_ = app.settings[ "display", "resolution" ].toStr.V2G;

		loadLibraries();
		createWindow();
	}
	void _step() {
		import std.datetime;
		StopWatch w;
		w.start();

		glClearColor( 0, 0, 0, 1.0 );
		glClearDepth( 0 );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		hookGLAppBeforeDraw.call();

		// So that sdl event hadling is as close as possible to drawing - low latency
		handleSDLEvents();

		// 2D draw
		{
			gl.viewMatrix = ViewMatrix.ortho( resolution );
			hook2DDraw.call();
			gl.finishDrawMode();
		}

		SDL_GL_SwapWindow( sdlWindow );

		import std.stdio;
		writeln( "Took ", w.peek().msecs, " ms, delta ", deltaTime * 1000, "ms" );

		checkForErrors();
	}
	void _uninit() {
		if( sdlWindow )
			SDL_DestroyWindow( sdlWindow );
		
		SDL_Quit();
		TTF_Quit();
		IMG_Quit();
	}

private:
	void loadLibraries() {
		DerelictSDL2.load();
		DerelictSDL2Image.load();
		DerelictSDL2ttf.load();

		DerelictGL3.load();

		enforce( SDL_Init( SDL_INIT_EVERYTHING ) == 0 );
		enforce( TTF_Init() == 0 );
		enforce( IMG_Init( IMG_INIT_JPG | IMG_INIT_PNG ) == ( IMG_INIT_JPG | IMG_INIT_PNG ) );
	}
	void createWindow() {
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 2 );
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 0 );
		SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
		SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );

		if( app.settings[ "display", "antialiasing" ].toBool ) {
			SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 1 );
			SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 4 );
		}
		
		uint flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN;
		sdlWindow = SDL_CreateWindow( "Window title".toStringz, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, cast( int ) resolution_.x, cast( int ) resolution_.y, flags );
		
		enforce( sdlWindow );
		
		glContext = SDL_GL_CreateContext( sdlWindow );
		enforce( glContext );
		
		GLVersion GLVer = DerelictGL3.reload();
		enforce( GLVer >= GLVersion.GL20, "Need higher GL version (current " ~ GLVer.to!string ~ ")" );

		glContext = SDL_GL_GetCurrentContext();
		
		SDL_GL_SetSwapInterval( 1 );
		
		gl.defaultBlendFunc();
	}
	void handleSDLEvents() {
		SDL_Event sdlEvent;
		
		while( SDL_PollEvent( &sdlEvent ) ) switch( sdlEvent.type ) {
			
			case SDL_QUIT:
				import std.stdio;
				app.stop();
				break;
				
			case SDL_WINDOWEVENT:
				switch( sdlEvent.window.event ) {
					
					case SDL_WINDOWEVENT_RESIZED:
						resolution = V2G( sdlEvent.window.data1, sdlEvent.window.data2 );
						break;
						
					case SDL_WINDOWEVENT_CLOSE:
						app.stop();
						break;
						
					default:
						break;
						
				}
				break;
				
			default:
				controlsMgr.handleSdlEvent( sdlEvent );
				break;
				
		}
	}

public:
	void checkForErrors() {
		auto sdlErr = SDL_GetError();
		if( sdlErr[ 0 ] != 0 ) {
			string errorStr = sdlErr.to!string;
			SDL_ClearError();
			throw new StrawException( "sdlError", "error", errorStr );
		}
		
		auto glErr = glGetError();
		if( glErr != GL_NO_ERROR ) {
			throw new StrawException( "glError", "error", glErr.to!string );
		}
	}

}

mixin _moduleInit!({
		hookAppInit.add( "renderer" ) = {
			if( !renderer )
				return;

			renderer._init();
			hookGLAppInit.call();
		};
		hookAppStep.add( "renderer" ) = {
			if( !renderer )
				return;

			renderer._step();
		};
		hookAppUninit.add( "renderer" ) = {
			if( !renderer )
				return;

			hookGLAppUninit.call();
			renderer._uninit();
			delete renderer;
		};
	});