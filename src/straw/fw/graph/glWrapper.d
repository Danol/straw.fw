﻿module straw.fw.graph.glWrapper;
version( graphics ):

import derelict.opengl3.gl3;
import straw.core;
import straw.core.math.viewMatrix;
import straw.fw.graph.drawBuffer;
import straw.fw.graph.renderer;
import straw.fw.res;
import straw.core.exception;
import straw.core.utils.general;
import straw.fw.graph.renderTarget;

enum GLP : ubyte {
	blend,
	cullFace,
	depthTest,
	scissor,
	
	_last
};
private enum GLPAssoc = [
	GL_BLEND,
	GL_CULL_FACE,
	GL_DEPTH_TEST,
	GL_SCISSOR_TEST
];
private enum GLPState : ubyte {
	unknown, on, off
}

struct GLWrapper {
	mixin( StrawObject );
	
public:
	alias DrawModeFunction = void delegate();
	
private:
	GLPState[ GLP._last ] glpVals = GLPState.unknown;
	GLuint boundTexture = -1, boundBuffer = -1;
	Shader boundShader;
	ubyte attribArrayCount_;

private:
	Container!(IRenderTarget, true) renderTargetStack;
	Container!ViewMatrix viewMatrixStack;
	
private:
	DrawModeFunction drawMode_;
	
public:
	DrawBuffer commonVertexBuffer, commonTextureCoordBuffer, commonColorBuffer;
	Shader colorShader, textureShader, fontShader, depthShader;

	ViewMatrix viewMatrix;
	
public:
	void delegate() defaultDrawMode_color, defaultDrawMode_texture; 
	
public:
	@disable this();
	this( None ) {
		defaultDrawMode_color = &defaultDrawMode_colorImpl;
		defaultDrawMode_texture = &defaultDrawMode_textureImpl;
	}
	~this() {
		delete commonColorBuffer;
		delete commonTextureCoordBuffer;
		delete commonVertexBuffer;
		
		// Not deleting shaders - those are deleted by ResourceManager
	}
	
public:
	@property void drawMode( DrawModeFunction set ) {
		if( drawMode_ is set ) return;
		
		if( drawMode_ )
			drawMode_();
		
		drawMode_ = set;
	}
	@property void setDrawMode_color() {
		drawMode = defaultDrawMode_color;
	}
	@property void setDrawMode_texture( Texture texture ) {
		if( drawMode_ is defaultDrawMode_texture && boundTexture != texture.textureId )
			finishDrawMode();
		else
			drawMode = defaultDrawMode_texture;
		
		this.texture = texture;
	}
	@property void setDrawMode_texture( GLuint textureId ) {
		if( drawMode_ is defaultDrawMode_texture && boundTexture != textureId )
			finishDrawMode();
		else
			drawMode = defaultDrawMode_texture;
		
		this.texture = textureId;
	}
	
	void finishDrawMode() {
		if( drawMode_ )
			drawMode_();
	}
	
public:
	@property GLuint texture() {
		return boundTexture;
	}
	/// !!! Does not call finishDrawMode(), you have to do it alone if necessary
	@property void texture( Texture set ) {
		GLuint id = set.textureId;
		if( boundTexture == id )
			return;
		
		glBindTexture( GL_TEXTURE_2D, id );
		boundTexture = id;
	}
	/// !!! Does not call finishDrawMode(), you have to do it alone if necessary
	@property void texture( GLuint set ) {
		if( boundTexture == set )
			return;
		
		glBindTexture( GL_TEXTURE_2D, set );
		boundTexture = set;
	}
	/// !!! Does not call finishDrawMode(), you have to do it alone if necessary
	@property void buffer( DrawBuffer set ) {
		GLuint id = set.bufferId;
		if( boundBuffer == id )
			return;
		
		glBindBuffer( GL_ARRAY_BUFFER, id );
		boundBuffer = id;
	}
	/// !!! Does not call finishDrawMode(), you have to do it alone if necessary
	@property void shader( Shader shader ) {
		if( boundShader is shader )
			return;
		
		glUseProgram( shader.programId );
		boundShader = shader;
	}
	@property Shader shader() {
		return boundShader;
	}
	
	void pushRenderTarget( IRenderTarget target ) {
		renderTargetStack ~= target;
		
		if( renderTargetStack.length >= 2 && renderTargetStack[ $ - 2 ].frameBufferId == target.frameBufferId )
			return;
		
		finishDrawMode();
		glBindFramebuffer( GL_FRAMEBUFFER, target.frameBufferId );
		
		pushMatrix();
		glViewport( 0, 0, cast( int ) target.size.x, cast( int ) target.size.y );
		viewMatrix = ViewMatrix.ortho( target.size );
	}
	void popRenderTarget() {
		if( !renderTargetStack.length )
			assert( 0, "No renderTarget to pop" );

		if( renderTargetStack.length >= 2 && renderTargetStack[ $ - 2 ] == renderTargetStack[ $ - 1 ] ) {
			renderTargetStack.popBack();
			popMatrix();
			return;
		}
		
		finishDrawMode();
		renderTargetStack.popBack();
		popMatrix();
		
		if( renderTargetStack.length ) {
			IRenderTarget target = renderTargetStack.back;
			glBindFramebuffer( GL_FRAMEBUFFER, target.frameBufferId );
			glViewport( 0, 0, cast( int ) target.size.x, cast( int ) target.size.y );
			
		}	else {
			glBindFramebuffer( GL_FRAMEBUFFER, 0 );
			glViewport( 0, 0, cast( int ) renderer.resolution.x, cast( int ) renderer.resolution.y );
			
		}
	}
	
	@property void attribArrayCount( ubyte set ) {
		while( set < attribArrayCount_ )
			glDisableVertexAttribArray( --attribArrayCount_ );
		
		while( set > attribArrayCount_ )
			glEnableVertexAttribArray( attribArrayCount_++ );
	}
	
private:
	void reset() {
		drawMode_ = null;
		
		foreach( ref glp; glpVals )
			glp = GLPState.unknown;
	}
	
public:
	void pushMatrix() {
		viewMatrixStack.pushBack( viewMatrix );
	}
	void popMatrix() {
		assert( viewMatrixStack.length );
		
		viewMatrix = viewMatrixStack.back;
		viewMatrixStack.popBack();
	}
	
public:
	ref This on( GLP[] glps ... ) {
		foreach( glp; glps ) {
			if( glpVals[ glp ] == GLPState.on )
				continue;
			
			glpVals[ glp ] = GLPState.on;
			glEnable( GLPAssoc[ glp ] );
		}
		
		return this;
	}
	ref This off( GLP[] glps ... ) {
		foreach( glp; glps ) {
			if( glpVals[ glp ] == GLPState.off )
				continue;
			
			glpVals[ glp ] = GLPState.off;
			glDisable( GLPAssoc[ glp ] );
		}
		
		return this;
	}
	
	void defaultBlendFunc() {
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	}
	
public:
	void defaultDrawMode_colorImpl() {
		drawMode_color( commonVertexBuffer, commonColorBuffer );
	}
	void defaultDrawMode_textureImpl() {
		drawMode_texture( commonVertexBuffer, commonTextureCoordBuffer );
	}
	
}

mixin _moduleInit!({
		hookLoadSystemResources.add() = {
			with( renderer.gl ) {
				commonVertexBuffer = new DrawBuffer();
				commonTextureCoordBuffer = new DrawBuffer();
				commonColorBuffer = new DrawBuffer();
				
				colorShader = Shader.createFromFile( "engine.color" );
				textureShader = Shader.createFromFile( "engine.texture" );
			}
		};
	});

void drawMode_color( DrawBuffer vertexBuffer, DrawBuffer colorBuffer ) {
	if( vertexBuffer.length == 0 )
		return;
	
	with( renderer.gl ) {
		on( GLP.blend );
		
		shader = colorShader;
		attribArrayCount = 2;
		
		glUniformMatrix4fv( shader.uniformId!"matrix", 1, GL_FALSE, viewMatrix.m.ptr );
		
		vertexBuffer.uploadAndUse!2( shader.attributeId!"vertex" );
		colorBuffer.uploadAndUse!4( shader.attributeId!"color" );
		
		glDrawArrays( GL_TRIANGLES, 0, cast( int )( vertexBuffer.length / 2 ) );
		
		vertexBuffer.clear();
		colorBuffer.clear();
	}
}
void drawMode_texture( DrawBuffer vertexBuffer, DrawBuffer textureCoordBuffer ) {
	if( vertexBuffer.length == 0 )
		return;
	
	with( renderer.gl ) {
		on( GLP.blend );
		
		shader = textureShader;
		attribArrayCount = 2;
		
		glUniformMatrix4fv( shader.uniformId!"matrix", 1, GL_FALSE, viewMatrix.m.ptr );
		
		vertexBuffer.uploadAndUse!2( shader.attributeId!"vertex" );
		textureCoordBuffer.uploadAndUse!2( shader.attributeId!"textureCoord" );
		
		glDrawArrays( GL_TRIANGLES, 0, cast( int )( vertexBuffer.length / 2 ) );
		
		vertexBuffer.clear();
		textureCoordBuffer.clear();
	}
}