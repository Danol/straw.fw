﻿module straw.fw.graph.color;

import std.conv;

struct Color {

public:
	float r = 0, g = 0, b = 0, a = 0; // floats because of opengl
	
public:
	this( ubyte grayscale, ubyte alpha = 255 ) {
		r = cast( float ) grayscale / 255;
		g = cast( float ) grayscale / 255;
		b = cast( float ) grayscale / 255;
		a = cast( float ) alpha / 255;
	}
	this( ubyte r, ubyte g, ubyte b, ubyte a = 255 ) {
		this.r = cast( float ) r / 255;
		this.g = cast( float ) g / 255;
		this.b = cast( float ) b / 255;
		this.a = cast( float ) a / 255;
	}
	this( float r, float g, float b, float a = 1 ) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	this( string str ) {
		fromString( str );
	}
	
public:
	void assign( Color c ) {
		r = c.r;
		g = c.g;
		b = c.b;
		a = c.a;
	}
	
public:
	void fromString( string source ) {
		if( source.length == 7 && source[ 0 ] == '#' ) {
			r = byteFromHex( source[ 1 .. 3 ] );
			g = byteFromHex( source[ 3 .. 5 ] );
			b = byteFromHex( source[ 5 .. 7 ] );
			a = 255;
			
		} else if( source.length == 9 && source[ 0 ] == '#' ) {
			r = byteFromHex( source[ 1 .. 3 ] );
			g = byteFromHex( source[ 3 .. 5 ] );
			b = byteFromHex( source[ 5 .. 7 ] );
			a = byteFromHex( source[ 7 .. 9 ] );
			
		} else {
			// TODO
			//assign( CL_assoc( source ) );
			assert( 0 );
			//return;
		}
		
		r /= 255;
		g /= 255;
		b /= 255;
		a /= 255;
		
		//global.strawLog( source ~ ": " ~ CL.assoc( source ).to!string );
	}
	string toString() {
		return "Color( " ~ r.to!string ~ ", " ~ g.to!string ~ ", " ~ b.to!string ~ ", " ~ a.to!string ~ " )";
	}
	
public:
	Color blend( ubyte alpha ) {
		return Color( r, g, b, a * cast( float ) alpha / 255 );
	}
	
public:
	bool opCast( T: bool )() {
		return a != 0;
	}
	
}

enum CL : Color {
	none = Color( 0, 0, 0, 0 ),
	
	white = Color( 255 ),
	black = Color( 0 ),
	
	red = Color( 255, 0, 0 ),
	green = Color( 0, 255, 0 ),
	blue = Color( 0, 0, 255 ),
}

ubyte byteFromHex( string str ) {
	assert( str.length >= 2 );
	ubyte r;
	
	if( str[ 0 ] >= '0' && str[ 0 ] <= '9' ) r += ( str[ 0 ] - '0' ) * 16;
	else if( str[ 0 ] >= 'a' && str[ 0 ] <= 'f' ) r += ( str[ 0 ] - 'a' + 10 ) * 16;
	else if( str[ 0 ] >= 'A' && str[ 0 ] <= 'F' ) r += ( str[ 0 ] - 'A' + 10 ) * 16;
	
	if( str[ 1 ] >= '0' && str[ 1 ] <= '9' ) r += ( str[ 1 ] - '0' );
	else if( str[ 1 ] >= 'a' && str[ 1 ] <= 'f' ) r += ( str[ 1 ] - 'a' + 10 );
	else if( str[ 1 ] >= 'A' && str[ 1 ] <= 'F' ) r += ( str[ 1 ] - 'A' + 10 );
	
	return r;
}