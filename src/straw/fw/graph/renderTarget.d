﻿module straw.fw.graph.renderTarget;

import derelict.opengl3.gl3;
import straw.core;
import straw.core.utils.typetuple;
import straw.fw.graph.renderer;
import straw.fw.res.texture;

// TODO Change view matrix when changing render target (depending on target's size)

package abstract class IRenderTarget {

protected:
	GLuint frameBufferId_;
	bool isResolutionSensitive;

package:
	/// Linked list
	IRenderTarget nextResolutionSensitiveRenderTarget, previousResolutionRenderTarget;

public:
	final @property GLuint frameBufferId() {
		return frameBufferId_;
	}

	abstract @property V2G size();

public:
	abstract void updateResolution();

}

final class RenderTarget( size_t colorBufferCount = 1, bool depthStencilBuffer = true ) : IRenderTarget {
	static assert( colorBufferCount > 0 );

public:
	alias This = typeof( this );

private:
	private static struct UseHelper {
		This target;
		~this() {
			target.pop();
		}
	}

private:
	enum colorAttachmentId = [
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3,
		GL_COLOR_ATTACHMENT4,
		GL_COLOR_ATTACHMENT5,
		GL_COLOR_ATTACHMENT6,
		GL_COLOR_ATTACHMENT7,
		GL_COLOR_ATTACHMENT8,
		GL_COLOR_ATTACHMENT9,
		GL_COLOR_ATTACHMENT10
	];

public:
	Texture[ colorBufferCount ] colorTexture;

	static if( depthStencilBuffer ) {
		GLuint depthStencilTextureId;

		alias depthTextureId = depthStencilTextureId;
		alias stencilTextureId = depthStencilTextureId;
	}

public:
	this( V2G size = 0.V2G ) {
		V2G actualSize = size;

		if( actualSize == 0.V2G ) {
			actualSize = renderer.resolution;
			isResolutionSensitive = true;

			nextResolutionSensitiveRenderTarget = renderer.firstResolutionSensitiveRenderTarget;
			if( nextResolutionSensitiveRenderTarget )
				nextResolutionSensitiveRenderTarget.previousResolutionRenderTarget = this;

			renderer.firstResolutionSensitiveRenderTarget = this;
		}

		glGenFramebuffers( 1, &frameBufferId_ );
		renderer.pushRenderTarget( this );

		foreach( i, ref tex; colorTexture ) {
			tex = Texture.create( actualSize, false );

			glFramebufferTexture2D( GL_FRAMEBUFFER, colorAttachmentId[ i ], GL_TEXTURE_2D, tex.textureId, 0 );
		}

		// Prepare depth/stencil buffer
		static if( depthStencilBuffer ) {
			glGenTextures( 1, &depthStencilTextureId );
			renderer.gl.texture = depthStencilTextureId;

			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

			glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
			glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE );

			//glTexParameteri( GL_TEXTURE_2D, GL_DEPTH_STENCIL_TEXTURE_MODE )

			glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, cast( GLsizei ) actualSize.x, cast( GLsizei ) actualSize.y, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, null );

			glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,	depthStencilTextureId, 0 );
			glFramebufferTexture2D( GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D,	depthStencilTextureId, 0 );
		}

		auto status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
		enforce( status == GL_FRAMEBUFFER_COMPLETE, new StrawException( "frameBufferCreateFail" ) );

		renderer.popRenderTarget();
	}
	~this() {
		foreach( tex; colorTexture )
			delete tex;

		static if( depthStencilBuffer )
			glDeleteTextures( 1, &depthStencilTextureId );

		glDeleteFramebuffers( 1, &frameBufferId_ );

		if( isResolutionSensitive ) {
			if( nextResolutionSensitiveRenderTarget )
				nextResolutionSensitiveRenderTarget.previousResolutionRenderTarget = previousResolutionRenderTarget;

			if( previousResolutionRenderTarget )
				previousResolutionRenderTarget.nextResolutionSensitiveRenderTarget = nextResolutionSensitiveRenderTarget;
			else
				renderer.firstResolutionSensitiveRenderTarget = nextResolutionSensitiveRenderTarget;
		}
	}

public:
	override @property V2G size() {
		return colorTexture[ 0 ] ? colorTexture[ 0 ].size : 0.V2G;
	}

	@property Texture texture() {
		return colorTexture[ 0 ];
	}

protected:
	override void updateResolution() {
		foreach( tex; colorTexture )
			tex.resize( renderer.resolution );

		static if( depthStencilBuffer ) {
			renderer.gl.texture = depthStencilTextureId;
			glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, cast( GLsizei ) renderer.resolution.x, cast( GLsizei ) renderer.resolution.y, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, null );
		}
	}

public:
	void push() {
		renderer.pushRenderTarget( this );
	}
	void pop() {
		renderer.popRenderTarget();
	}

	/// To be used lie this: with( target.use() ) { /* code which is rendered to the target */ }
	UseHelper use() {
		push();
		return UseHelper( this );
	}

}