﻿module straw.fw.graph.drawBuffer;
version( graphics ):

import derelict.opengl3.gl;
import straw.core.storage.container;
import straw.core.math.vector;
import straw.fw.graph.color;
import straw.fw.graph.renderer;

static assert( is( GLfloat == float ), "GLfloat is not float, must do something with that dude!" );
static assert( is( V2G.T == float ), "V2G is not of float, must do something with that dude!" );

final class DrawBuffer {

private:
	alias T = V2G.T;

	package( straw.fw )
		GLuint bufferId;

	Container!T data;

public:
	this() {
		glGenBuffers( 1, &bufferId );
	}
	~this() {
		// TODO - delete buffer list
		glDeleteBuffers( 1, &bufferId );
	}

public:
	void bind() {
		renderer.gl.buffer = this;
	}
	void upload() {
		bind();
		glBufferData( GL_ARRAY_BUFFER, data.length * float.sizeof, data.ptr, GL_DYNAMIC_DRAW );
	}
	/// DOES NOT UPLOAD, only binds
	void use( ubyte batchSize )( GLuint useAs ) {
		bind();
		glVertexAttribPointer( useAs, batchSize, GL_FLOAT, GL_FALSE, 0, null );
	}
	void uploadAndUse( ubyte batchSize )( GLuint useAs ) {
		upload();
		glVertexAttribPointer( useAs, batchSize, GL_FLOAT, GL_FALSE, 0, null );
	}
	
	void clear() {
		data.clear();
	}
	
	@property size_t length() {
		return data.length;
	}
	
public:
	void push( T val ) {
		data.pushBack( val );
	}
	void push( V2G pt ) {
		data.pushBack( pt.x, pt.y );
	}
	void push( V3G pt ) {
		data.pushBack( pt.x, pt.y, pt.z );
	}
	void push( Color c ) {
		data.pushBack( c.r, c.g, c.b, c.a );
	}

	void pushNTimes( size_t times )( T val ) {
		data.pushBackNTimes!times( val );
	}
	void pushNTimes( size_t times )( Color c ) {
		data.pushBackNTimes!times( c.r, c.g, c.b, c.a );
	}

	void pushTriangle( V2G p1, V2G p2, V2G p3 ) {
		data.pushBack( p1.x, p1.y, p2.x, p2.y, p3.x, p3.y );
	}
	void pushTriangle( V3G p1, V3G p2, V3G p3 ) {
		data.pushBack( p1.x, p1.y, p1.z, p2.x, p2.y, p2.z, p3.x, p3.y, p3.z );
	}

	void pushQuad( V2G lt, V2G rt, V2G lb, V2G rb ) {
		data.pushBack(
			lt.x, lt.y,
			rt.x, rt.y,
			rb.x, rb.y,

			lt.x, lt.y,
			rb.x, rb.y,
			lb.x, lb.y
			);
	}
	void pushQuad( V3G lt, V3G rt, V3G lb, V3G rb ) {
		data.pushBack(
			lt.x, lt.y, lt.z,
			rt.x, rt.y, rt.z,
			rb.x, rb.y, rb.z,
			
			lt.x, lt.y, lt.z,
			rb.x, rb.y, rb.z,
			lb.x, lb.y, lb.z
			);
	}

	/// Quite specific one, I know... I just... can't help myself
	/// Pushes a line-quad (hard to explain, just two triangles that together form a line)
	void pushZeroWidthQuad( V3G v1, V3G v2 ) {
		data.pushBack(
			v1.x, v1.y, v1.z,
			v2.x, v2.y, v2.z,
			v2.x, v2.y, v2.z,
			
			v1.x, v1.y, v1.z,
			v2.x, v2.y, v2.z,
			v1.x, v1.y, v1.z
			);
	}
	
	void pushRect( V2G lt, V2G size ) {
		data.pushBack(
			lt.x, lt.y,
			lt.x + size.x, lt.y,
			lt.x + size.x, lt.y + size.y,
			
			lt.x, lt.y,
			lt.x + size.x, lt.y + size.y,
			lt.x, lt.y + size.y
			);
	}
	void pushRect( V3G lt, V2G size ) {
		data.pushBack(
			lt.x, lt.y, lt.z,
			lt.x + size.x, lt.y, lt.z,
			lt.x + size.x, lt.y + size.y, lt.z,
			
			lt.x, lt.y, lt.z,
			lt.x + size.x, lt.y + size.y, lt.z,
			lt.x, lt.y + size.y, lt.z
			);
	}

	void pushRectLTRB( V2G lt, V2G rb ) {
		data.pushBack(
			lt.x, lt.y,
			rb.x, lt.y,
			rb.x, rb.y,
			
			lt.x, lt.y,
			rb.x, rb.y,
			lt.x, rb.y
			);
	}

	void pushData( Args ... )( Args args )
		if( __traits( compiles, data.pushBack( args ) ) )
	{
		data.pushBack( args );
	}

}