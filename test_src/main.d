﻿module main;

import std.math;
import straw.core.hooks;
import straw.fw.app.app;
import straw.fw.graph.color;
import straw.fw.graph.renderer;
import straw.fw.res;
import straw.fw.ctrl;

final class App : Application {

public:
	this() {
		renderer = new Renderer();
	}

}

void main() {
	_moduleInit__call();

	App app = new App;
	app.startHere();
	delete app;
}

__gshared {
	Texture tex;
	ControlHandler keyTest, keyTest2;
}

mixin _moduleInit!({
		hookLoadSystemResources.add() = {
			tex = Texture.createFromFile( "hulls.png", false );

			keyTest.setCombination( "keyboard.a" );
			keyTest2.setCombination( "keyboard.a", "keyboard.b" );
		};
		hook2DDraw.add() = {
			keyTest2.step();
			keyTest.step();

			if( keyTest2.isPressed )
				renderer.draw.rectangle( 10.V2G, 50.V2G, CL.white );

			if( keyTest.isPressed )
				renderer.draw.rectangle( 20.V2G, 50.V2G, Color( 255, 0, 0, 128 ) );

			tex.advancedDraw( 200.V2G ).scale( tex.size * ( 0.75 + sin( app.tickCounter / 10.0 ) / 4 ) ).finish();
			tex.advancedDraw( 400.V2G ).part( tex.size * ( 1 + sin( app.tickCounter / 10.0 ) / 3 ) ).flip( sin( app.tickCounter / 20.0 ) > 0, sin( app.tickCounter / 40.0 ) > 0 ).finish();
		};
	});